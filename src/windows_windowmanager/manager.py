
import re
import platform
if platform.system() == "Windows":
    import win32gui
    import win32process
    import win32com.client
    import wmi


class WindowMgr:
    """Encapsulates some calls to the winapi for window management"""

    WIN32PROCESS_QUERY = "SELECT Name FROM Win32_Process WHERE ProcessId = %s" 

    def __init__ (self):
        """Constructor"""
        self._handle = None
	self.matches = []
        self.shell = win32com.client.Dispatch("WScript.Shell")
        self.c = wmi.WMI()

    def find_window(self, class_name, window_name = None):
        """find a window by its class_name"""
        self._handle = win32gui.FindWindow(class_name, window_name)


    def _window_enum_callback_process_name(self, hwnd, wildcard):
        '''Pass to win32gui.EnumWindows() to check all the opened windows'''
        try:
	    _, pid = win32process.GetWindowThreadProcessId( hwnd )
	    for p in self.c.query( self.WIN32PROCESS_QUERY % ( str(pid), ) ):        
                if re.match( wildcard, str( p.Name ) ) != None:
	            self.matches.append( ( hwnd, str( p.Name ), ) )
        except Exception, e:
	    raise (e)
	    pass
        pass

    def _window_enum_callback_window_title( self, hwnd, wildcard ):
        handler_text = win32gui.GetWindowText( hwnd )
        if re.match( wildcard, handler_text ) != None:
	    self.matches.append( ( hwnd, handler_text ) )
        pass

    def match_windows_wildcard( self, wildcard, search_by = "process_name" ):
        """ Adds all matches of searching handlers by process name\
	or by window title
	to the matches list
	"""
	self.matches = []
	if search_by == "process_name":
	    win32gui.EnumWindows( self._window_enum_callback_process_name, wildcard)
	    pass
	if search_by == "window_title":
            win32gui.EnumWindows(self._window_enum_callback_window_title, wildcard)
	return self.matches
        pass


    def find_window_wildcard(self, wildcard, search_by = "process_name" ):
        """ Finds all windows that match the wildcard and if
	there are results it sets the first one as the handle.
	"""
        self._handle = None
        self.match_windows_wildcard( wildcard, search_by=search_by )
	if len( self.matches ) == 0:
	    raise Exception( "No windows matched the wildcard: %s" % ( wildcard,) )
	self._handle = self.matches[ 0 ][ 0 ]
        if not self._handle:
	    raise BaseException( "Window not found" )
        pass


    def set_foreground( self, handler=None ):
        """put the window in the foreground"""
	if not handler:
	    win32gui.SetForegroundWindow( self._handle )
	else:
	    win32gui.SetForegroundWindow( handler )
	    


    def get_pid( self, handle ):
        """ Gets the Process Id from a handle.
	"""
        return win32process.GetWindowThreadProcessId( handle )
        pass

    def activate_window( self, pid ):
        """ Activates the received pid via a WScript shell
	"""
        self.shell.AppActivate( pid )
        pass

    def sendkeys( self, string):
        self.shell.SendKeys( string )
        pass


