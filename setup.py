
from distutils.core import setup

setup(
        name='windows_windowmanager',
        version='0.0.1',
	description="A class for searching and getting windows guis handlers",
        author="Chaps",
        author_email="drumchaps@gmail.com",
        maintainer="Chaps",
        maintainer_email="drumchaps@gmail.com",
        url="https://bitbucket.org/drumchaps/windows_windowmanager",
        py_modules=[
        ],   
        packages  = [
            "windows_windowmanager",
        ],
        package_dir={'windows_windowmanager': 'src/windows_windowmanager'},
)
