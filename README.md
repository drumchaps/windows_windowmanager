

A class for searching and getting a window handler
via win32gui


Use:

```python
from windows_windowmanager.manager import WindowMgr
wm = WindowMgr()
#Find all Explorer handler matches.
wm.find_window_wildcard(".*explorer.*")
#Print all the wildcard matches
print wm.matches
#Bring all matches to foreground
for m in wm.matches:
    wm.set_foreground( m[0] )
```
